# Project Butthead

## Idea

As in "What are you looking at, butthead" from Back to the Future. And I'm the butthead, teaching himself React with silly projects like this (that will probably piss off Back to the Future nerds).

Here is the premise.  For a long time, I would have a rough estimate in my head of what time I would need to leave the house to get to work or what time to leave work to make it to something else.  Then I would get distracted doing something and make compromises in order to finish that something (ie. "if I don't fill the gas tank, I have ten more minutes", or "uh oh, I guess I can't stop at 7-11 on the way home").  I thought I could make myself an timer that would give me a comfortable-time-to-leave reminder, a dangerously-close-to-being-late warning, and a start-thinking-up-excuses warning.

I know this isn't how the train or DeLorean from Back to the Future worked, but I thought the colored sticks used in the locomotive, the thermometer in the locomotive, and the DeLorean's time readouts lended themselves perfectly to representing what I wanted to do.

## Using the App

The app loads with the start time being the current time and destination being an hour from then.  Click the readouts to open an modal to change those respective times.

The three inputs at the bottom represent when the warnings will occur in relation to the duration spelled out above.

Thats it. I hope to add some more styling, actual alerts, and some logic to prevent it breaking in the future.  

## Standard React App Documentation (..that I don't want to delete yet)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

#### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

#### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

#### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

#### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

#### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

#### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

#### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
