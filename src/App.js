import React from 'react';
import moment from 'moment';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import './App.scss';

import TimeOutput from './TimeOutput';
import TimeAlarms from './TimeAlarms';
import TimeReadout from './TimeReadout';

class App extends React.Component {

	constructor() {
		super()
		this.state = {
			nowDate: moment().toDate(),
			startDate: moment().toDate(),
			destDate:  moment().add(2,'hours').toDate(),
			stick1: 60,
			stick2: 30,
			stick3: 15
		}
		this.onStick1Change = this.onStick1Change.bind(this);
		this.onStick2Change = this.onStick2Change.bind(this);
		this.onStick3Change = this.onStick3Change.bind(this);
		this.handleStartChange = this.handleStartChange.bind(this);
		this.handleDestChange = this.handleDestChange.bind(this);
	}

	componentDidMount() {
		this.interval = setInterval(() => this.setState({ nowDate: moment().toDate() }), 10000);
	}
	componentWillUnmount() {
		clearInterval(this.interval);
	}

	onStick1Change(stick1) {
		this.setState({stick1: parseInt(stick1)})
	}
	onStick2Change(stick2) {
		this.setState({stick2:  parseInt(stick2)})
	}
	onStick3Change(stick3) {
		this.setState({stick3:  parseInt(stick3)})
	}
	
	handleStartChange(date) {
		this.setState({
			startDate: date
		});
	}

	handleDestChange(date) {
		this.setState({
			destDate: date
		});
	}
	
	render() {

		return (
			<div className="back-to-work">
				<div className="time-readout-wrapper container">
					<TimeOutput 
						timeNow={this.state.nowDate} 
						timeStart={this.state.startDate} 
						timeDest={this.state.destDate} 
						stick1={this.state.stick1} 
						stick2={this.state.stick2}  
						stick3={this.state.stick3} 
					/>
				</div>
				<div className="time-input-wrapper container metal linear">
					<div className="time-input time-input__start">
						<TimeReadout time={this.state.startDate} />
						<DatePicker
							selected={this.state.startDate}
							onChange={this.handleStartChange}
							timeIntervals={5}
							showTimeSelect
						/>
						<div className="rowSticker"><span>Start Time</span></div>
					</div>

					<div className="time-input time-input__start">
						<TimeReadout time={this.state.destDate} />
						<DatePicker
							selected={this.state.destDate}
							onChange={this.handleDestChange}
							timeIntervals={15}
							showTimeSelect
						/>
						<div className="rowSticker"><span>Destination Time</span></div>
					</div>
					
					
					<TimeAlarms 
						stick1={this.state.stick1}
						onStick1Change={this.onStick1Change}
						stick2={this.state.stick2}
						onStick2Change={this.onStick2Change}
						stick3={this.state.stick3}
						onStick3Change={this.onStick3Change}
					/>
				</div>
			</div>
		);
	}
}

export default App;
