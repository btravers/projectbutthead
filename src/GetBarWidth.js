import React from 'react'

import moment from 'moment'

function GetBarWidth(props) {
    
        const stickMin = moment.duration(props.timeDuration, 'minutes')
        const percentage = stickMin / props.timeStartToEnd
    
        const dashOffsetVariable = 728.84888 // 2 * π * R  = Circumference of svg circle
        const modDashOffset = dashOffsetVariable * 0.75
        const dashOffsetStart = -(dashOffsetVariable * 0.25) // To make animation go counter clockwise, need to edit dashoffset
        

        const dashOffset = dashOffsetStart-(modDashOffset.toFixed(2) * (1 - percentage.toFixed(2)))


        const styles = {
            strokeDashoffset: `${dashOffset}`
        }

    return (
        <circle className={'progress-' + props.barColor} style={styles} cx="175" cy="175" r="116"/>
    )
}

export default GetBarWidth