import React from 'react'

function GetTickerPosition(props) {
          
        const percentage = props.timeStartToNow / props.timeStartToEnd
        const degrees = -(percentage * 270)

        const rotate = 'rotate(' + degrees + 'deg)'

        const styles = {
            transform: `${rotate}`
        }

    return (
        <g id="ticker" className="ticker-wrapper" style={styles}>
            <path className="ticker-brown" d="M176.46,29.95c0-0.86-0.01-16.44-0.01-17.32h-2.9c0,0.88-0.01,16.46-0.01,17.32
                c0,53.21-15.54,40.82-15.54,62.75c0,8.89,6.84,16.15,15.54,16.89l0.01,55.24h2.9l0.01-55.21c8.7-0.74,15.54-8.03,15.54-16.92
                C192,70.76,176.46,83.16,176.46,29.95z M175,106.97c-7.88,0-14.27-6.39-14.27-14.27s6.39-14.27,14.27-14.27
                c7.88,0,14.27,6.39,14.27,14.27S182.88,106.97,175,106.97z"/>
            <path className="ticker-brown" d="M194.68,261.29c0-10.38-8.04-18.86-18.23-19.61l0.01-55.41l-2.91-0.03l0.01,55.44
                c-10.19,0.75-18.23,9.23-18.23,19.61c0,2.19,0.37,4.29,1.03,6.25c0-0.01,0-0.01,0-0.02c0-10.3,8.35-18.65,18.65-18.65
                c10.3,0,18.65,8.35,18.65,18.65c0,0.01,0,0.01,0,0.02C194.31,265.58,194.68,263.48,194.68,261.29z"/>
            <circle className="ticker-brown" cx="175" cy="175" r="15"/>
            <circle cx="175" cy="175" r="7.5"/>
        </g>
    )
}

export default GetTickerPosition