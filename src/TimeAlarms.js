import React from 'react'

class TimeAlarms extends React.Component {
	constructor(props) {
		super(props)

		this.handleStick1Change = this.handleStick1Change.bind(this);
		this.handleStick2Change = this.handleStick2Change.bind(this);
		this.handleStick3Change = this.handleStick3Change.bind(this);

	}

	handleStick1Change(e) {
		this.props.onStick1Change(e.target.value);
	}
	handleStick2Change(e) {
		this.props.onStick2Change(e.target.value);
	}
	handleStick3Change(e) {
		this.props.onStick3Change(e.target.value);
	}

	render(props) {
		const stick1 = this.props.stick1;
		const stick2 = this.props.stick2;
		const stick3 = this.props.stick3;
		return (
			<div id="timeAlarms" className="row">
				<div id="stick1" className="stick col">
					<div className="stick-hdr"><span>1</span></div>
						<input type="number" className="display" value={stick1} onChange={this.handleStick1Change} />
					<div className="timeAlarms-below">minutes before #2</div>
				</div>
				<div id="stick2" className="stick col">
					<div className="stick-hdr"><span>2</span></div>
						<input type="number" className="display" value={stick2} onChange={this.handleStick2Change} />
					<div className="timeAlarms-below">minutes before #3</div>
				</div>
				<div id="stick3" className="stick col">
					<div className="stick-hdr"><span>3</span></div>
						<input type="number" className="display" value={stick3} onChange={this.handleStick3Change} />
					<div className="timeAlarms-below">minutes before dest.</div>
				</div>
			</div>
		)
	}
}

export default TimeAlarms;