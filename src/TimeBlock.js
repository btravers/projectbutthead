import React from 'react'

class TimeBlock extends React.Component {

    render(props) {
        return (
            <div id={'timeStart-' + this.props.timeSection} className="col-sm-2">
                <div className="sticker"><span>{this.props.timeSection}</span></div>
                <div className="display">{this.props.variable}</div>
            </div>
        )
    }
}

export default TimeBlock;