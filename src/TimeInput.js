import React from 'react'

import TimeAlarms from './TimeAlarms';
import TimeReadout from './TimeReadout';

class TimeInput extends React.Component {
	render(props) {
		return (
			<div>
				<TimeReadout position="Start" time={this.props.timeStart} />
				<TimeReadout position="Destination" time={this.props.timeDest} />
				<TimeAlarms 
					stick1={this.props.stick1}
					onStick1Change={this.props.onStick1Change}
					stick2={this.props.stick2}
					stick3={this.props.stick3} 
				/>
			</div>
		);
	}
}

export default TimeInput;