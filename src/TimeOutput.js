import React from 'react'
import moment from 'moment'
import GetBarWidth from './GetBarWidth';
import GetTickerPosition from './GetTickerPosition';

function TimeOutput(props) {

	const startMin = moment(props.timeStart, 'minutes');
	const nowtMin = moment(props.timeNow, 'minutes');
	const destMin = moment(props.timeDest, 'minutes');
	const startToEnd = destMin.diff(startMin);
	const startToNow = destMin.diff(nowtMin);
	
	return (
		<div className="meter-wrapper row">
			<div className="meter-backer metal radial"></div>
		
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 350 350">
				<circle id="meter-base" cx="175" cy="175" r="160"/>
				<g id="progress-meter" className="progress-clip-wrapper">
					<clipPath id="progress-clip">
						<circle id="progress-clip" cx="175" cy="175" r="116"/>
					</clipPath>
					<g id="white-wrapper" className="meter-clip">
						<circle className="progress-white" cx="175" cy="175" r="116"/>
					</g>
					<g id="green-wrapper" className="meter-clip">
						<GetBarWidth barColor="green"  
							timeStartToEnd={startToEnd}
							timeDuration={props.stick1} 
						/>
					</g>
					<g id="yellow-wrapper" className="meter-clip">
						<GetBarWidth barColor="yellow"  
							timeStartToEnd={startToEnd}
							timeDuration={props.stick2} 
						/>
					</g>
					<g id="red" className="meter-clip">
						<GetBarWidth barColor="red"  
							timeStartToEnd={startToEnd}
							timeDuration={props.stick3} 
						/>
					</g>
				</g>
				<g className="ticker-wrapper">
					<GetTickerPosition 
						timeStartToNow={startToNow} 
						timeStartToEnd={startToEnd} 
					/>
				</g>
				<g id="ticks">
					<path className="tick" d="M277.1,277.8c26.43-26.25,42.8-62.61,42.8-102.8c0-80.02-64.87-144.89-144.89-144.89S30.11,94.98,30.11,175 c0,40.19,16.37,76.56,42.8,102.8"/>
					<line className="tick" x1="57.78" y1="89.83" x2="62.05" y2="92.94"/>
					<line className="tick" x1="80.9" y1="64.83" x2="84.33" y2="68.84"/>
					<line className="tick" x1="109.21" y1="45.87" x2="111.6" y2="50.58"/>
					<line className="tick" x1="141.17" y1="34.09" x2="142.4" y2="39.22"/>
					<line className="tick" x1="208.83" y1="34.09" x2="207.6" y2="39.22"/>
					<line className="tick" x1="240.79" y1="45.87" x2="238.4" y2="50.58"/>
					<line className="tick" x1="269.1" y1="64.83" x2="265.67" y2="68.84"/>
					<line className="tick" x1="292.22" y1="89.83" x2="287.95" y2="92.94"/>
					<line className="tick" x1="31.88" y1="152.33" x2="37.09" y2="153.16"/>
					<line className="tick" x1="35.81" y1="185.95" x2="30.55" y2="186.37"/>
					<line className="tick" x1="42.19" y1="218.15" x2="37.17" y2="219.78"/>
					<line className="tick" x1="51.45" y1="250.71" x2="55.95" y2="247.96"/>
					<line className="tick" x1="312.91" y1="153.16" x2="318.12" y2="152.33"/>
					<line className="tick" x1="319.45" y1="186.37" x2="314.19" y2="185.95"/>
					<line className="tick" x1="312.83" y1="219.78" x2="307.81" y2="218.15"/>
					<line className="tick" x1="298.55" y1="250.71" x2="294.05" y2="247.96"/>
					<line className="tick" x1="277.45" y1="277.45" x2="273.72" y2="273.72"/>
					<line className="tick" x1="72.55" y1="277.45" x2="76.28" y2="273.72"/>
					<line className="tick" x1="41.11" y1="119.54" x2="45.99" y2="121.56"/>
					<line className="tick" x1="175" y1="30.11" x2="175" y2="35.39"/>
					<line className="tick" x1="308.89" y1="119.54" x2="304.01" y2="121.56"/>
				</g>
			</svg>
		</div>
	);
}

export default TimeOutput;