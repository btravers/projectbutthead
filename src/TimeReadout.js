import React from 'react'
import moment from 'moment'

function TimeReadout(props) {
    const timeInput = props.time

    const timeMonth = moment(timeInput).format("MMM")
    const timeDay = moment(timeInput).format("DD")
    const timeYear = moment(timeInput).format("YYYY")
    const timeAmPm = moment(timeInput).format("a")
    const timeHour = moment(timeInput).format("hh")
    const timeMin = moment(timeInput).format("mm")


    return (
        <div id="timeStart" className="readout-wrapper">

            <svg className="readout-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="1000px" height="200px" viewBox="0 0 1000 200">
            <g id="month">
                <rect y="60" width="194.2" height="130"/>
                <text transform="matrix(1 0 0 1 17.4502 155.604)" className="st0 st1 st2">888</text>
                <text transform="matrix(1 0 0 1 17.4502 155.604)" className="st3 st1 st2">{timeMonth}</text>
                <rect x="42" y="13" className="st3" width="105" height="35"/>
                <text x="94" y="37" className="label-text">month</text>
            </g>
            <g id="day">
                <rect x="220" y="60" width="135" height="130"/>
                <text transform="matrix(1 0 0 1 246.9067 155.604)" className="st0 st1 st2">88</text>
                <text transform="matrix(1 0 0 1 246.9067 155.604)" className="st3 st1 st2">{timeDay}</text>
                <rect x="255" y="13" className="st3" width="60" height="35"/>
                <text x="287" y="37" className="label-text">day</text>
            </g>
            <g id="year">
                <rect x="385" y="60" width="235" height="130"/>
                <text transform="matrix(1 0 0 1 403.0622 155.604)" className="st0 st1 st2">8888</text>
                <text transform="matrix(1 0 0 1 403.0622 155.604)" className="st3 st1 st2">{timeYear}</text>
                <rect x="465" y="13" className="st3" width="82" height="35"/>
                <text  x="507" y="37" className="label-text">year</text>
            </g>
            <g id="ampm" className={timeAmPm}>
                <rect x="626" y="51" className="st3" width="51" height="35"/>
                <rect x="626" y="122" className="st3" width="51" height="35"/>
                <text x="652" y="78" className="label-text">AM</text>
                <text x="652" y="148" className="label-text">PM</text>
                <circle className="ampm-light am-light" cx="652" cy="104" r="11"/>
                <circle className="ampm-light pm-light" cx="652" cy="172" r="11"/>
            </g>
            <g id="hour">
                <rect x="686.8" y="60" width="135" height="130"/>
                <text transform="matrix(1 0 0 1 707.929 155.604)" className="st0 st1 st2">88</text>
                <text transform="matrix(1 0 0 1 707.929 155.604)" className="st3 st1 st2">{timeHour}</text>
                <rect x="712" y="13" className="st3" width="78" height="35"/>
                <text  x="754" y="37" className="label-text">hour</text>
            </g>
            <g id="min">
                <rect x="865" y="60" width="135" height="130"/>
                <text transform="matrix(1 0 0 1 886.5333 155.604)" className="st0 st1 st2">88</text>
                <text transform="matrix(1 0 0 1 886.5333 155.604)" className="st3 st1 st2">{timeMin}</text>
                <rect x="894" y="13" className="st3" width="63" height="35"/>
                <text  x="928" y="37" className="label-text">min</text>
            </g>
            </svg>
        </div>
    )
}

export default TimeReadout